﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuCanvas : MonoBehaviour
{
    [SerializeField] private Dropdown _dropdown;
    [SerializeField] private Button _startButton;
    [SerializeField] private Text _placeholderText;
    [SerializeField] private Text _inputFieldText;
    [SerializeField] private InputField _inputField;

    void Start()
    {
        _dropdown.image.sprite = _dropdown.options[_dropdown.value].image;
        _inputField.text = PlayerPrefs.GetString(KeyLibrary.KeyNamePlayer);
    }

    void OnEnable()
    {
        _dropdown.onValueChanged.AddListener(OnValueChanged_OnChangeSprite);

        _startButton.onClick.AddListener(()=>
        {
            StartButtonOnClick_OnChangeScene();
            SoundManager.Instance.PlaySFX(SFX.Click);
        });
    }

    void OnDisable()
    {
        _dropdown.onValueChanged.RemoveListener(OnValueChanged_OnChangeSprite);
        _startButton.onClick.RemoveListener(StartButtonOnClick_OnChangeScene);
    }

    private void OnValueChanged_OnChangeSprite(int index)
    {
        _dropdown.image.sprite = _dropdown.options[_dropdown.value].image;
    }

    private void StartButtonOnClick_OnChangeScene()
    {
        if (_inputField.text.Length > 8)
        {
            _inputFieldText.color = Color.red;
        }
        else
        {
            _inputFieldText.color = Color.black;
        }

        if (_inputField.text == "")
        {
            _placeholderText.color = Color.red;
        }

        if (_inputField.text != "" && _inputField.text.Length <= 8)
        {
            PlayerPrefs.SetString(KeyLibrary.KeyNamePlayer, _inputField.text);
            PlayerPrefs.Save();

            string selectedСolor = _dropdown.captionText.text;

            PlayerPrefs.SetString(KeyLibrary.KeySelectedСolor, selectedСolor);
            PlayerPrefs.Save();

            SceneManager.LoadScene(KeyLibrary.NameGameScene);
        }
    }
}
