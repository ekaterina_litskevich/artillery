﻿using UnityEngine;
using System;

public abstract class Being : MonoBehaviour
{
    public static event Action OnShot; 

    protected const float Speed = 4.0f;
    private const float JumpPower = 7.0f;

    [SerializeField] protected SpriteRenderer _spriteRenderer;
    [SerializeField] protected Rigidbody2D _rigidbody2d;
    [SerializeField] protected LineRenderer _lineRenderer;

    [SerializeField] private WeaponSource _weaponSource;
    [SerializeField] private Transform _weaponAnchor;

    protected Weapon _weapon = null;

    protected Vector2 _direction;

    protected int _directionInput;
    protected int _hp = 100;

    protected bool _isGrounded = true;
    protected bool _isFacingRight = true;
    protected bool _isHisTurn = false;

    public int HP
    {
        get => _hp;
        set => _hp = value;
    }

    public void StopMove()
    {
        _isHisTurn = false;
        RemoveWeapon();
    }

    public virtual void StartMove()
    {
        _isHisTurn = true;
    }

    protected void CallEvent()
    {
        OnShot?.Invoke();
    }

    protected virtual void Move(int inputAxis)
    {
        _directionInput = inputAxis;
    }

    protected virtual void Jump()
    {
        if (_isGrounded && _isHisTurn)
        {
            _rigidbody2d.velocity = new Vector2(_rigidbody2d.velocity.x, JumpPower);
            _isGrounded = false;
            SoundManager.Instance.PlaySFX(SFX.Jump);
        }
    }

    protected virtual void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision != null)
        {
            if (collision.gameObject.TryGetComponent(out Platform platform))
            {
                _isGrounded = true;
            }
        }
    }

    protected void Flip()
    {
        _isFacingRight = !_isFacingRight;
        transform.eulerAngles += Vector3.up * 180.0f;
        _lineRenderer.transform.eulerAngles += Vector3.up * 180.0f;
    }

    protected virtual void Update()
    {
        if (!_isHisTurn)
            return;

        if ((_directionInput < 0 && _isFacingRight) || (_directionInput > 0 && !_isFacingRight))
        {
            Flip();
        }

        if (_weapon != null)
        {
            if ((_isFacingRight && _weapon.Direction.x < 0) || (!_isFacingRight && _weapon.Direction.x > 0))
            {
                Flip();
            }
        }
    }

    protected virtual void FixedUpdate()
    {
        if (!_isHisTurn)
            return;

        _rigidbody2d.velocity = new Vector2(Speed * _directionInput, _rigidbody2d.velocity.y);
    }

    protected void StopAim()
    {
        _lineRenderer.positionCount = 0;
    }


    public void SetWeapon(WeaponType weaponType)
    {
        if (_weapon != null)
        {
            RemoveWeapon();
        }

        Weapon weapon = _weaponSource.GetWeapon(weaponType);
        _weapon = Instantiate(weapon, _weaponAnchor);

        _weapon.LineRenderer = _lineRenderer;
    }

    protected void RemoveWeapon()
    {
        StopAim();

        if (_weapon != null)
        {
            Destroy(_weapon.gameObject);
            _weapon = null;
        }
    }
}
