﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ButtonController : MonoBehaviour
{
    public static event Action <int> OnClickMoveButton;
    public static event Action OnClickJumpButton;

    [SerializeField] private PlayerButton _leftArrowButton;
    [SerializeField] private PlayerButton _rightArrowButton;
    [SerializeField] private PlayerButton _upArrowButton;

    [SerializeField] private Button _weaponChangeButtton;

    [SerializeField] private WeaponPopup _weaponPopup;


    private bool _isWeaponPopupLocked = false;

    private void OnEnable()
    {
        _leftArrowButton.OnButtonDown += LeftArrowButton_OnButtonDown;
        _rightArrowButton.OnButtonDown += RightArrowButton_OnButtonDown;
        _upArrowButton.OnButtonDown += UpArrowButton_OnButtonDown;

        _leftArrowButton.OnButtonUp += LeftArrowButton_OnButtonUp;
        _rightArrowButton.OnButtonUp += RightArrowButton_OnButtonUp;
        _upArrowButton.OnButtonUp += UpArrowButton_OnButtonUp;

        _weaponChangeButtton.onClick.AddListener(WeaponChangeButtton_OnClick);
    }

    private void OnDisable()
    {
        _leftArrowButton.OnButtonDown -= LeftArrowButton_OnButtonDown;
        _rightArrowButton.OnButtonDown -= RightArrowButton_OnButtonDown;
        _upArrowButton.OnButtonDown -= UpArrowButton_OnButtonDown;

        _leftArrowButton.OnButtonUp -= LeftArrowButton_OnButtonUp;
        _rightArrowButton.OnButtonUp -= RightArrowButton_OnButtonUp;
        _upArrowButton.OnButtonUp -= UpArrowButton_OnButtonUp;

        _weaponChangeButtton.onClick.RemoveListener(WeaponChangeButtton_OnClick);
    }

    public void HideWeaponPopupAndLocked()
    {
        _weaponPopup.Hide();
        _isWeaponPopupLocked = true;
    }

    public void UnLockedWeaponPopup()
    {
        _isWeaponPopupLocked = false;
    }

    private void LeftArrowButton_OnButtonDown()
    {
        OnClickMoveButton?.Invoke(-1);
    }  

    private void LeftArrowButton_OnButtonUp()
    {
        OnClickMoveButton?.Invoke(0);
    }

    private void RightArrowButton_OnButtonDown()
    {
        OnClickMoveButton?.Invoke(1);
    }

    private void RightArrowButton_OnButtonUp()
    {
        OnClickMoveButton?.Invoke(0);
    }

    private void UpArrowButton_OnButtonDown()
    {
        OnClickJumpButton?.Invoke();
    }

    private void UpArrowButton_OnButtonUp()
    {
        OnClickJumpButton?.Invoke();
    }

    private void WeaponChangeButtton_OnClick()
    {
        if (!_isWeaponPopupLocked)
        {
            _weaponPopup.Show();
        }
    }
}
