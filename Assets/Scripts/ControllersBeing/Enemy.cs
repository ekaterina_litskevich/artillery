﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Enemy : Being
{
    private const float DirectionMultiplier = 4.0f;
    private const float ValueToChangeDirection = 0.03f;

    [SerializeField] private Transform _raycastRightAnchor;

    private Character _character;

    private Vector3 _lastPoint;

    private float _startPosition = 0f;
    private float _minDistanceToPlayer = 2.0f;
    private float _maxDistanceTraveled = 3.0f;

    private bool _isMadeMove;
    private bool _isShot = false;
    private bool _isAssignStartPosition = false;
    private bool _isJamp = false;
    private bool _isAiming = false;

    public override void StartMove()
    {
        base.StartMove();
        _isMadeMove = true;
    }
    protected override void Move(int inputAxis)
    {
        base.Move(inputAxis);
    }

    protected override void Jump()
    {
        base.Jump();
        _rigidbody2d.velocity = new Vector2(Speed / 1.5f * _directionInput, _rigidbody2d.velocity.y);
    }

    public void AssignCharacter(Character character)
    {
        _character = character;
    }

    private IEnumerator ChoiceOfWeapons()
    {
        _isAiming = true;

        Move(0);

        List<int> amount = new List<int>();

        foreach (int val in Enum.GetValues(typeof(WeaponType)))
        {
            amount.Add(val);
        }

        for (int i = 1; i < amount.Count; i++)
        {
            SetWeapon((WeaponType)i);

            float directionX = _character.transform.position.x - transform.position.x;
            float directionY = _character.transform.position.y - transform.position.y;

            Vector2 maxDirection = new Vector2(directionX, directionY) * DirectionMultiplier;
            Vector2 minDirection = new Vector2(directionX, directionY) / DirectionMultiplier;

            float pointX = UnityEngine.Random.Range(minDirection.x, maxDirection.x);
            float pointY = UnityEngine.Random.Range(minDirection.y, maxDirection.y);

            _direction = new Vector2(pointX, pointY);

            Aim();
            yield return new WaitForSeconds(2.0f);

            if (!_isShot && _isHisTurn)
            {
                float distanceX = _lastPoint.x - _character.transform.position.x;
                float distanceY = _lastPoint.y - _character.transform.position.y;

                bool isIncrease;

                for (int j = 0; j < 200; j++)
                {
                    isIncrease = false;

                    if ((distanceX > 0 && _isFacingRight) || (distanceX < 0 && !_isFacingRight))
                    {
                        ChangeDirection(isIncrease, ValueToChangeDirection, 0);

                        if (_isShot)
                        {
                            break;
                        }
                    }

                    if ((distanceY > 0 && !_isFacingRight) || (distanceY < 0 && _isFacingRight))
                    {
                        ChangeDirection(isIncrease, 0, ValueToChangeDirection);

                        if (_isShot)
                        {
                            break;
                        }
                    }

                    yield return null;

                    isIncrease = true;

                    if ((distanceX > 0 && !_isFacingRight) || (distanceX < 0 && _isFacingRight))
                    {
                        ChangeDirection(isIncrease, ValueToChangeDirection, 0);

                        if (_isShot)
                        {
                            break;
                        }
                    }

                    if ((distanceY > 0 && _isFacingRight) || (distanceY < 0 && !_isFacingRight))
                    {
                        ChangeDirection(isIncrease, 0, ValueToChangeDirection);

                        if (_isShot)
                        {
                            break;
                        }
                    }


                }
            }

            if (!_isHisTurn)
            {
                RemoveWeapon();
                StopCoroutine(ChoiceOfWeapons());
                break;
            }

            if (_isShot)
            {
                StopCoroutine(ChoiceOfWeapons());
                break;
            }
        }

        if (!_isShot)
        {
            RemoveWeapon();

            float distance = transform.position.x - _character.transform.position.x;

            if (distance > 0)
            {
                base.Move(-1);
            }
            else
            {
                base.Move(1);
            }

            _isAssignStartPosition = true;
        }

        _isAiming = false;
    }

    private void ChangeDirection(bool isIncrease, float x, float y)
    {
        if (isIncrease)
        {
            _direction += new Vector2(x, y);
        }
        else
        {
            _direction -= new Vector2(x, y);
        }

        Aim();
    }

    private void Aim()
    {
        if (_weapon != null)
        {
            _lastPoint = _weapon.Aiming(_direction, out Collider2D collider);

            if (collider != null)
            {
                if (collider.TryGetComponent(out Character character))
                {
                    CallEvent();
                    _weapon.Shot();
                    StopAim();
                    _isShot = true;
                }
                else
                {
                    _isShot = false;
                }
            }
        }  
    }

    private void Turn()
    {
        float distance = transform.position.x - _character.transform.position.x;
        if (distance > 0 && _isFacingRight)
        {
            Flip();
        }
        if (distance < 0 && !_isFacingRight)
        {
            Flip();
        }
    }

    protected override void OnCollisionEnter2D(Collision2D collision)
    {
        base.OnCollisionEnter2D(collision);
        _isJamp = false;
    }

    protected override void Update()
    {
        if (_isHisTurn)
        {

            base.Update();

            if (_directionInput != 0)
            {
                float distanceToPlayer = transform.position.x - _character.transform.position.x;

                if (distanceToPlayer < 0)
                {
                    distanceToPlayer *= -1;
                }

                if ((distanceToPlayer <= _minDistanceToPlayer) && !_isJamp)
                {
                    Move(0);
                    SetWeapon(WeaponType.ShotGun);
                    StartCoroutine(ChoiceOfWeapons());
                }

                if (distanceToPlayer > _minDistanceToPlayer)
                {
                    if (_isAssignStartPosition)
                    {
                        _startPosition = transform.position.x;
                        _isAssignStartPosition = false;
                    }

                    float distanceTraveled = _startPosition - transform.position.x;

                    if (distanceTraveled < 0)
                    {
                        distanceTraveled *= -1;
                    }

                    if (distanceTraveled  >=  _maxDistanceTraveled)
                    {
                        if (!_isJamp)
                        {
                            StartCoroutine(ChoiceOfWeapons());
                        }
                    }
                }
            }

            
            Turn();

            RaycastHit2D hit;
            if (_isFacingRight)
            {
                Debug.DrawRay(_raycastRightAnchor.position, Vector2.right * 0.5f, Color.blue);
                hit = Physics2D.Raycast(_raycastRightAnchor.position, Vector2.right, 0.5f);
            }
            else
            {
                Debug.DrawRay(_raycastRightAnchor.position, Vector2.left * 0.5f, Color.blue);
                hit = Physics2D.Raycast(_raycastRightAnchor.position, Vector2.left, 0.5f);
            }

            if (_isMadeMove)
            {
                _isShot = false; 
                StartCoroutine(ChoiceOfWeapons());
            }


            if (hit.collider != null)
            {
                if (hit.collider.TryGetComponent(out Platform platform) && !_isAiming)
                {
                    _isJamp = true;
                    Jump();
                }
            }

            _isMadeMove = false;
        }
    }

    protected override void FixedUpdate()
    {
        if (!_isJamp)
        {
            base.FixedUpdate();
        }
    }
}
