﻿using UnityEngine;
using UnityEngine.EventSystems;

public class Character : Being
{
    private Camera _camera;

    private Vector2 _startTouchPosition;

    private bool _isAiming;

    private void Start()
    {
        string selectedСolor = PlayerPrefs.GetString(KeyLibrary.KeySelectedСolor);
        if (ColorUtility.TryParseHtmlString(selectedСolor, out Color color))
        {
            _spriteRenderer.color = color;
        }

        _camera = Camera.main;
    }
    private void OnEnable()
    {
        ButtonController.OnClickMoveButton += Move;
        ButtonController.OnClickJumpButton += Jump;
        WeaponPopup.OnWeaponChange += SetWeapon;
    }

    private void OnDisable()
    {
        ButtonController.OnClickMoveButton -= Move;
        ButtonController.OnClickJumpButton -= Jump;
        WeaponPopup.OnWeaponChange -= SetWeapon;
    }

    protected override void Update()
    {
        if (_isHisTurn)
        {
            base.Update();
            if (_weapon != null)
            {
                Vector2 mousePosition = _camera.ScreenToWorldPoint(Input.mousePosition);
                
                if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())
                {
                    Vector3 touchPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

                    RaycastHit2D hit = Physics2D.Raycast(Camera.main.transform.position, touchPosition);

                    if (hit.collider != null)
                    {
                        if (hit.collider.TryGetComponent(out Character character))
                        {
                            _startTouchPosition = _camera.ScreenToWorldPoint(Input.mousePosition);
                            _isAiming = true;
                        }
                    }
                }

                if (Input.GetMouseButton(0) && !EventSystem.current.IsPointerOverGameObject() && _isAiming)
                {
                    _direction = _startTouchPosition - mousePosition;

                    _weapon.Aiming(_direction, out Collider2D collider);
                }

                if (Input.GetMouseButtonUp(0) && !EventSystem.current.IsPointerOverGameObject() && _isAiming)
                {
                    if (_weapon != null)
                    {
                        CallEvent();
                        _weapon.Shot();
                        StopAim();
                        _isAiming = false;
                    }
                }
            }
        }
    }
}