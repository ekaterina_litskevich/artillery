﻿public enum GameState
{
    None = 0,
    Play = 2,
    Lose = 3, 
    Win = 4
}
