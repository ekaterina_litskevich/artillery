﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Launcher : MonoBehaviour
{
    private void Start()
    {
        AdsController.Instance.Init();

        SceneManager.LoadScene(KeyLibrary.NameMainMenuScene);
    }
}
