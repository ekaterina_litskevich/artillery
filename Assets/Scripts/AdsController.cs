﻿using GoogleMobileAds.Api;
using System;
using UnityEngine;

public class AdsController : MonoBehaviourSingleton<AdsController>
{
    private event Action OnReward;

    private const string AdUnitID = "ca-app-pub-3940256099942544/5224354917";

    private RewardedAd _rewardedAd;
    private bool _isInitialize = false;

    protected override void SingletonStarted() 
    {
        if (!_isInitialize)
        {
            Init();
        }
    }


    public void Init()
    {
        _isInitialize = true;

        MobileAds.Initialize(initStatus =>{});

        _rewardedAd = new RewardedAd(AdUnitID);

        LoadReward();

        _rewardedAd.OnUserEarnedReward += RewardedAd_OnUserEarnedReward;
        _rewardedAd.OnAdClosed += RewardedAd_OnAdClosed;
    }

    protected override void SingletonDestroyed()
    {
        _rewardedAd.OnUserEarnedReward -= RewardedAd_OnUserEarnedReward;
        _rewardedAd.OnAdClosed -= RewardedAd_OnAdClosed;
    }

    public void ShowRewardVideo(Action action)
    {
        OnReward = action;

        if (_rewardedAd.IsLoaded())
        {
            _rewardedAd.Show();
        }
    }

    private void LoadReward()
    {
        AdRequest request = new AdRequest.Builder().Build();
        _rewardedAd.LoadAd(request);
    }

    private void RewardedAd_OnUserEarnedReward(object sender, Reward e)
    {
        UnityMainThreadDispatcher.Instance().Enqueue(() =>
        {
            OnReward?.Invoke();

            LoadReward();
        });
    }

    private void RewardedAd_OnAdClosed(object sender, EventArgs e)
    {
        UnityMainThreadDispatcher.Instance().Enqueue(() =>
        {
            LoadReward();
        });
    }
}
