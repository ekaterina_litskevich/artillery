﻿using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class GameCanvas : MonoBehaviour
{
    public event Action<int> OnShowRewardVideo;

    private const int RewardForWatchingVideo = 50;

    [SerializeField] private Text _namePlayerText;
    [SerializeField] private Slider _hpCharacterSlider;
    [SerializeField] private Slider _hpEnemySlider;
    [SerializeField] private Button _exitMenuLoseButton;
    [SerializeField] private Button _exitMenuWinButton;
    [SerializeField] private Button _reclameButton;
    [SerializeField] private Image _loseMenuImage;
    [SerializeField] private Image _winMenuImage;

    private void Start()
    {
        _namePlayerText.text = PlayerPrefs.GetString(KeyLibrary.KeyNamePlayer);
    }

    private void OnEnable()
    {
        _exitMenuLoseButton.onClick.AddListener(() =>
        {
            ChangeScene();
            SoundManager.Instance.PlaySFX(SFX.Click);
        });

        _exitMenuWinButton.onClick.AddListener(()=>
        {
            ChangeScene();
            SoundManager.Instance.PlaySFX(SFX.Click);
        });
        _reclameButton.onClick.AddListener(ViewingAds);
    }

    private void OnDisable()
    {
        _exitMenuLoseButton.onClick.RemoveListener(ChangeScene);
        _exitMenuWinButton.onClick.RemoveListener(ChangeScene);
        _reclameButton.onClick.RemoveListener(ViewingAds);
    }

    public void ChangeCharacterSlider(int hp)
    {
        _hpCharacterSlider.value = hp;
    }

    public void ChangeEnemySlider(int hp)
    {
        _hpEnemySlider.value = hp;
    }

    public void TurnOnLoseMenu()
    {
        _loseMenuImage.gameObject.SetActive(true);

        _reclameButton.transform.DOBlendableLocalMoveBy(Vector3.right * 10.0f, 1.0f).SetEase(Ease.InOutElastic).SetLoops(-1,LoopType.Yoyo);
    }

    public void TurnOnWinMenu()
    {
        _winMenuImage.gameObject.SetActive(true);
    }

    private void ChangeScene()
    {
        SceneManager.LoadScene(KeyLibrary.NameMainMenuScene);
    }

    private void ViewingAds()
    {
        AdsController.Instance.ShowRewardVideo(AdsController_ShowRewardVideo);
    }

    private void AdsController_ShowRewardVideo()
    {
        _hpCharacterSlider.value += RewardForWatchingVideo;
        _loseMenuImage.gameObject.SetActive(false);

        OnShowRewardVideo?.Invoke(RewardForWatchingVideo);
    }
}
