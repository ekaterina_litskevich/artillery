﻿using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private const float boundWight = 1;

    [SerializeField] private TimerControl _timerControl;
    [SerializeField] private ButtonController _buttonController;
    [SerializeField] private GameCanvas _gameCanvas;
    [SerializeField] private Camera _cameraMain;

    [SerializeField] private Character _characterPrefab;
    [SerializeField] private Enemy _enemyPrefab;

    [SerializeField] private List<Transform> _spanwPoints = new List<Transform>();

    private List<Transform> _availableSpawnPoint = new List<Transform>();

    private Queue<Being> _moveSequence = new Queue<Being>();

    private Being _currentBeing;
    private Character _character;

    private Vector3 _bounds;

    private GameState _gameState;

    private void OnEnable()
    {
        _timerControl.OnTimeIsUp += TimerControl_OnTimeIsUp;

        _gameCanvas.OnShowRewardVideo += _gameCanvas_OnShowRewardVideo;

        Being.OnShot += Being_OnShot;

        Weapon.OnDestroyLastBullet += Weapon_OnDestroyLastBullet;
        MachineGun.OnDestroyBullet += MachineGun_OnDestroyBullet;
    }

    private void _gameCanvas_OnShowRewardVideo(int rewardForWatchingVideo)
    {
        _character.HP += rewardForWatchingVideo;

        _timerControl.StartTimer();
        SetCurrentBeing();

        _gameState = GameState.Play;
    }

    private void OnDisable()
    {
        _timerControl.OnTimeIsUp -= TimerControl_OnTimeIsUp;

        _gameCanvas.OnShowRewardVideo -= _gameCanvas_OnShowRewardVideo;

        Being.OnShot -= Being_OnShot;

        Weapon.OnDestroyLastBullet -= Weapon_OnDestroyLastBullet;
        MachineGun.OnDestroyBullet -= MachineGun_OnDestroyBullet;
    }

    private void Start()
    {
        _gameState = GameState.Play;

        _availableSpawnPoint = new List<Transform>(_spanwPoints);

        _character = Instantiate(_characterPrefab, transform);
        Vector3 positionCharacter = GetSpawnPoint();
        _character.transform.position = positionCharacter;

        Enemy enemy = Instantiate(_enemyPrefab, transform);
        Vector3 positionEnemy = GetSpawnPoint();
        enemy.transform.position = positionEnemy;
        enemy.AssignCharacter(_character);

        _moveSequence.Enqueue(_character);
        _moveSequence.Enqueue(enemy);

        _bounds = _cameraMain.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height));
        CreateBorders();

        SetCurrentBeing();
    }

    private void Weapon_OnDestroyLastBullet(Being being, int damage)
    {
        ChangeHPBeing(being, damage);

        if (_gameState == GameState.Play)
        {
            _timerControl.StartTimer();
            SetCurrentBeing();
        }
    }

    private void MachineGun_OnDestroyBullet(Being being, int damage)
    {
        ChangeHPBeing(being, damage);
    }

    private void ChangeHPBeing(Being being, int damage)
    {
        if (being != null)
        {
            being.HP -= damage;
            if (being is Character)
            {
                _gameCanvas.ChangeCharacterSlider(being.HP);

                if ((being.HP <= 0))
                {
                    _gameState = GameState.Lose;
                    StopGame();
                    _gameCanvas.TurnOnLoseMenu();
                }
            }
            else
            {
                _gameCanvas.ChangeEnemySlider(being.HP);

                if ((being.HP <= 0))
                {
                    _gameState = GameState.Win;
                    StopGame();
                    _gameCanvas.TurnOnWinMenu();
                }
            }
        }
    }

    private void StopGame()
    {
        _timerControl.StopTimer();

        foreach (var item in _moveSequence)
        {
            item.StopMove();
        }
    }

    private void TimerControl_OnTimeIsUp()
    {
        SetCurrentBeing();

        _timerControl.StartTimer();
    }

    private void Being_OnShot()
    {
        _timerControl.StopTimer();
    }

    private void SetCurrentBeing()
    {
        Being being = _moveSequence.Dequeue();
        _currentBeing = being;
        _moveSequence.Enqueue(being);

        foreach (var item in _moveSequence)
        {
            item.StopMove();
        }

        SoundManager.Instance.PlaySFX(SFX.ChangeMove);
        _currentBeing.StartMove();

        if (being is Character)
        {
            _buttonController.UnLockedWeaponPopup();
        }
        else
        {
            _buttonController.HideWeaponPopupAndLocked();
        }
    }

    private Vector3 GetSpawnPoint()
    { 
        int index = Random.Range(0, _availableSpawnPoint.Count - 1);

        Vector3 spawnPointPosition = _availableSpawnPoint[index].position;

        _availableSpawnPoint.RemoveAt(index);

        return spawnPointPosition;
    }

    private void CreateBorders()
    {
        GameObject borders = new GameObject("Borders");

        GameObject borderUp = new GameObject("BorderUp");
        borderUp.transform.SetParent(borders.transform);
        BoxCollider2D colliderUp = borderUp.AddComponent<BoxCollider2D>();
        colliderUp.size = new Vector3(_bounds.x * 2, boundWight);
        borderUp.transform.position = new Vector3(0, _bounds.y + (boundWight / 2.0f));

        GameObject borderDown = new GameObject("BorderDown");
        borderDown.transform.SetParent(borders.transform);
        BoxCollider2D colliderDown = borderDown.AddComponent<BoxCollider2D>();
        colliderDown.size = new Vector3(_bounds.x * 2, boundWight);
        borderDown.transform.position = new Vector3(0, -_bounds.y - (boundWight / 2.0f));

        GameObject borderRight = new GameObject("BorderRight");
        borderRight.transform.SetParent(borders.transform);
        BoxCollider2D colliderRight = borderRight.AddComponent<BoxCollider2D>();
        colliderRight.size = new Vector3(boundWight, _bounds.y * 2);
        borderRight.transform.position = new Vector3(_bounds.x + (boundWight / 2.0f), 0);

        GameObject borderLeft = new GameObject("BorderLeft");
        borderLeft.transform.SetParent(borders.transform);
        BoxCollider2D colliderLeft = borderLeft.AddComponent<BoxCollider2D>();
        colliderLeft.size = new Vector3(boundWight, _bounds.y * 2);
        borderLeft.transform.position = new Vector3(-_bounds.x - (boundWight / 2.0f), 0);
    }
}
