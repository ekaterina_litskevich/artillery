﻿public static class SFX
{
    public const string ChangeMove = "ChangeMove";
    public const string Click = "Click";
    public const string Jump = "Jump";
    public const string Shot = "Shot";
}
