﻿using UnityEngine;

public class SoundManager : MonoBehaviourSingleton<SoundManager>
{
    [SerializeField] AudioSource audioSourceSFX = null;

    [SerializeField] SoundSource soundSourceSFX = null;

    public void PlaySFX(string soundName)
    {
        AudioClip audioClip = soundSourceSFX.GetSound(soundName);
        if (audioClip != null)
        {
            audioSourceSFX.PlayOneShot(audioClip);
        }
    }
}
