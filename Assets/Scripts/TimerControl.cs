﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class TimerControl : MonoBehaviour
{
    public event Action OnTimeIsUp;

    private const float Second = 15;

    [SerializeField] private Text _timerText;

    private float second = 15;

    private bool _isStop = false;

    public void StartTimer()
    {
        second = Second;
        _isStop = false;
    }

    public void StopTimer()
    {
        _isStop = true;
    }

    private void ChangeText()
    {
        if (second >= 10)
        {
            _timerText.text = string.Format("00:{0}", (int)second);
        }
        else
        {
            _timerText.text = string.Format("00:0{0}", (int)second);
        }
    }

    private void Update()
    {
        if (!_isStop)
        {
            ChangeText();

            if (second <= 0)
            {
                OnTimeIsUp?.Invoke();
            }
            second -= Time.deltaTime;
        }
    }
}
