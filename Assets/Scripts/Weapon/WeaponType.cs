﻿public enum WeaponType 
{
    None = 0,
    SniperRifle = 1,
    MachineGun = 2,
    ShotGun = 3
}