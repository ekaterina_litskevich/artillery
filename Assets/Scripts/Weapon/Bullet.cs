﻿using System.Collections;
using UnityEngine;
using System;

public class Bullet : MonoBehaviour
{
    public event Action<Being> OnDestroy;

    [SerializeField] private Rigidbody2D _rigidbody2d;

    public void Move(Vector3 direction, int lifeTime)
    {
        _rigidbody2d.AddForce(direction, ForceMode2D.Impulse);
        StartCoroutine(BulletLifeTime(lifeTime));
    }

    private IEnumerator BulletLifeTime(int lifeTime)
    {
        float second = lifeTime * 0.0001f;

        yield return new WaitForSeconds(second);

        OnDestroy?.Invoke(null);
        Destroy(gameObject);
    }

    private void Update()
    {
        Collider2D collider2D = Physics2D.OverlapPoint(transform.position);
        if (collider2D != null)
        {
            if (collider2D.TryGetComponent(out Being being))
            {
                OnDestroy?.Invoke(being);
                Destroy(gameObject);
            }
        }
    }
}
