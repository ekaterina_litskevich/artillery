﻿using System.Collections;
using UnityEngine;

public class SniperRifle : Weapon
{
    public override void Shot()
    {
        StartCoroutine(DelayShot());
        SoundManager.Instance.PlaySFX(SFX.Shot);
    }

    private IEnumerator DelayShot()
    {
        yield return new WaitForSeconds(1.0f);

        RaycastHit2D hit = Physics2D.Raycast(_firePoint.position, _direction * _weaponSettings.ProjectileSpeed, _weaponSettings.ProjectileLifeTime);
        if (hit.collider != null)
        {
            if (hit.collider.TryGetComponent(out Being being))
            {
                Bullet_OnDestroy(being);
            }
            else
            {
                Bullet_OnDestroy(null);
            }
        }
    }
}
