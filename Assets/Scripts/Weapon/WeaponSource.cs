﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[CreateAssetMenu(fileName = "WeaponSource", menuName = "Weapon/WeaponSource")]
public class WeaponSource : ScriptableObject
{
    [SerializeField] List<Weapon> _weaponList = new List<Weapon>();

    public Weapon GetWeapon(WeaponType weaponType)
    {
        return _weaponList.FirstOrDefault(t => t.WeaponType == weaponType);
    }
}
