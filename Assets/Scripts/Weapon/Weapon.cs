﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class Weapon : MonoBehaviour
{
    public static event Action<Being, int> OnDestroyLastBullet;

    [SerializeField] protected WeaponSettings _weaponSettings;
    [SerializeField] protected Transform _firePoint;
    [SerializeField] protected Transform _weaponSprite;
    [SerializeField] protected Bullet _bulletPrefab;

    protected Vector3 _forceSchot = Vector3.zero;
    protected Vector2 _direction;
    private Vector2 _gravity;
    public Vector2 Direction => _direction;
    public WeaponType WeaponType => _weaponSettings.WeaponType;

    private LineRenderer _lineRenderer;
    public LineRenderer LineRenderer
    {
        get => _lineRenderer;
        set => _lineRenderer = value;
    }

    public Transform WeaponSprite
    {
        get => _weaponSprite;
        set => _weaponSprite = value;
    }

    private void Start()
    {
        _gravity = Physics2D.gravity;
    }

    public Vector3 Aiming(Vector2 direction, out Collider2D collider)
    {
        Vector3 lastPoint = Vector3.zero;
        collider = null;

        _direction = direction;

        Vector3 rotation = new Vector3(0, 0, -Quaternion.LookRotation(_direction).eulerAngles.x);
        _weaponSprite.transform.localEulerAngles = rotation;

        _lineRenderer.transform.position = _firePoint.position;

        List<Vector3> points = new List<Vector3>();

        for (int i = 0; i < _weaponSettings.ProjectileLifeTime; i++)
        {
            float time = i * 0.0001f;

            Vector2 point = _direction * _weaponSettings.ProjectileSpeed * time + _gravity * time * time / 2f;
            Collider2D collider2D = Physics2D.OverlapPoint(_lineRenderer.transform.TransformPoint(point));

            if (collider2D != null)
            {
                collider = collider2D;
                if (collider2D.TryGetComponent(out Being being) || collider2D.TryGetComponent(out Platform platform))
                {
                    lastPoint = _lineRenderer.transform.TransformPoint(point);
                    break;
                }
            }
            points.Add(point);
        }

        _lineRenderer.positionCount = points.Count;
        _lineRenderer.SetPositions(points.ToArray());

        return lastPoint;
    }

    public virtual void Shot()
    {
        Bullet bullet = Instantiate(_bulletPrefab, transform);
        bullet.transform.position = _firePoint.position;

        bullet.Move(_direction * _weaponSettings.ProjectileSpeed, _weaponSettings.ProjectileLifeTime);

        bullet.OnDestroy += Bullet_OnDestroy;
    }

    protected virtual void Bullet_OnDestroy(Being being)
    {
        OnDestroyLastBullet?.Invoke(being, _weaponSettings.ProjectileDamage);
    }
}
