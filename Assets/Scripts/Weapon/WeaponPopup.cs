﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponPopup : MonoBehaviour
{
    public static event Action<WeaponType> OnWeaponChange;

    [SerializeField] private Transform _root;
    [SerializeField] private Button _weaponButtonPrototype;
    [SerializeField] private Image _weaponImagePrototype;

    [SerializeField] List<WeaponButton> _weaponButtons = new List<WeaponButton>();

    private void Start()
    {
        for (int i = 0; i < _weaponButtons.Count; i++)
        {
            WeaponButton weaponButton = _weaponButtons[i];
            _weaponImagePrototype.sprite = weaponButton.WeaponSprite;
            Button button = Instantiate(_weaponButtonPrototype, _root);
            
            button.onClick.AddListener(() =>
            {
                OnWeaponChange?.Invoke(weaponButton.WeaponType);
                Hide();
            });
        }

        Hide();
    }

    public void Show()
    {
        _root.gameObject.SetActive(true);
    }
    
    public void Hide()
    {
        _root.gameObject.SetActive(false);
    }

    [Serializable]
    private class WeaponButton
    {
        [SerializeField] WeaponType _weaponType;
        [SerializeField] Sprite _weaponSprite;

        public WeaponType WeaponType => _weaponType;
        public Sprite WeaponSprite => _weaponSprite;
    }
}