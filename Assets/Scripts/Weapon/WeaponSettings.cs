﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "WeaponSettings", menuName = "Weapon/WeaponSettings")]
public class WeaponSettings : ScriptableObject
{
    [SerializeField] private WeaponType _weaponType;
    [SerializeField] private float _projectileSpeed;
    [SerializeField] private int _projectileDamage;
    [SerializeField] private int _projectileLifeTime;

    public WeaponType WeaponType => _weaponType;
    public float ProjectileSpeed => _projectileSpeed;
    public int ProjectileDamage => _projectileDamage;
    public int ProjectileLifeTime => _projectileLifeTime;
}
