﻿using System.Collections;
using UnityEngine;

public class ShotGun : Weapon
{
    public override void Shot()
    {
        StartCoroutine(DelayShot());
        SoundManager.Instance.PlaySFX(SFX.Shot);
    }

    private IEnumerator DelayShot()
    {
        yield return new WaitForSeconds(1.0f);

        base.Shot();
    }
}
