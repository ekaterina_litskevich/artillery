﻿using System.Collections;
using UnityEngine;
using System;

public class MachineGun : Weapon
{
    public static event Action<Being, int> OnDestroyBullet;

    private int _registrelShot = 0;

    private IEnumerator SeriesOfBulletsCoroutine()
    {
        for (int i = 0; i < 3; i++)
        {
            base.Shot();
            SoundManager.Instance.PlaySFX(SFX.Shot);

            yield return new WaitForSeconds(0.1f);
        }
    }

    protected override void Bullet_OnDestroy(Being being)
    {
        _registrelShot++;

        if (_registrelShot == 3)
        {
            base.Bullet_OnDestroy(being);
        }
        else
        {
            OnDestroyBullet?.Invoke(being, _weaponSettings.ProjectileDamage);
        }
    }

    public override void Shot()
    {
        StartCoroutine(SeriesOfBulletsCoroutine());
    }
}
